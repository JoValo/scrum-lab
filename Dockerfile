FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app

RUN apt-get update
RUN apt-get -y install curl
RUN apt-get -y install jq

COPY . .

RUN curl -Ls -o codacy-coverage-reporter "$(curl -Ls https://api.github.com/repos/codacy/codacy-coverage-reporter/releases/latest | jq -r '.assets | map({name, browser_download_url} | select(.name | contains("codacy-coverage-reporter-linux"))) | .[0].browser_download_url')"
RUN chmod +x codacy-coverage-reporter

RUN dotnet tool install --global coverlet.console --version 1.3.0
RUN dotnet restore
