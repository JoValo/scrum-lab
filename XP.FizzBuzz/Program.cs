﻿using System;

namespace XP.FizzBuzz
{
  /// <summary>
  /// Program.
  /// </summary>
  public class Program
  {
    #region :: Methods ::

    /// <summary>
    /// The entry point of the program, where the program control starts and ends.
    /// </summary>
    /// <param name="args">The command-line arguments.</param>
    static void Main(string[] args)
    {
      try
      {
        Console.WriteLine("Limite Inferior: ");
        var minLimit = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Limite Superior: ");
        var maxLimit = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine($"Inferior : {minLimit} - Superior {maxLimit}");

        DoFizzBuzz(minLimit, maxLimit);
      }
      catch (Exception ex)
      {
        Console.WriteLine($"Error -> {ex.Message}");
      }
    }

    #endregion

    #region :: Private Methods ::

    /// <summary>
    /// Dos the fizz buzz.
    /// </summary>
    /// <param name="min">Minimum.</param>
    /// <param name="max">Max.</param>
    public static void DoFizzBuzz(int min, int max)
    {
      if (max < min || min < 0 || max < 0)
      {
        throw new ArgumentException("Arguments no valids");
      }

      for (int i = min; i <= max; i++)
      {
        bool fizz = i % 3 == 0;
        bool buzz = i % 5 == 0;
        if (fizz && buzz)
          Console.WriteLine("FizzBuzz");
        else if (fizz)
          Console.WriteLine("Fizz");
        else if (buzz)
          Console.WriteLine("Buzz");
        else
          Console.WriteLine(i);
      }
    }

    #endregion
  }
}
