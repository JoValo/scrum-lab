using System;
using Xunit;
using FluentAssertions;

namespace XP.UniTests
{
  public class UnitTest1
  {
    [Fact]
    public void DoFizzBuzz_OK_Test()
    {
      Action act = () => FizzBuzz.Program.DoFizzBuzz(1, 100);
      act.Should().NotThrow();
    }

    [Fact]
    public void DoFizzBuzz_Fail_Test()
    {
      Action act = () => FizzBuzz.Program.DoFizzBuzz(100, 1);
      act.Should().Throw<ArgumentException>();
    }
  }
}
